package mail;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Mail {

	@NotNull
	@Email
    private String setTo;

    @NotNull
    @Email
    private String setFrom;
    
    @NotNull
    private String subject;

    @NotNull
    @Min(10)
    private String text;

	public String getSetTo() {
		return setTo;
	}

	public void setSetTo(String setTo) {
		this.setTo = setTo;
	}

	public String getSetFrom() {
		return setFrom;
	}

	public void setSetFrom(String setFrom) {
		this.setFrom = setFrom;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

    
}
