package pfe.prj.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfe.prj.dao.PackRepository;

@Service
public class PackMetierImpl implements PackMetier{
	
   @Autowired
   private PackRepository packrepository;

   
   @Override
	public List<Long> getPackProduct(Long pack_id) {
		List<Long> list=packrepository.getPackProduct(pack_id);
		 System.out.println(list.toString());
		 return list;
	}

}
