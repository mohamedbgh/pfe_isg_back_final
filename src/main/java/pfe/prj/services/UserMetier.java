package pfe.prj.services;

import java.util.List;

import pfe.prj.entities.User;

public interface UserMetier {
	public User saveUser(User u);
	public List<User> listeUser();
	public User FindUserByUsername(String username);
}
