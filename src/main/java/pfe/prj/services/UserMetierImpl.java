package pfe.prj.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfe.prj.dao.UserRepository;
import pfe.prj.entities.User;
@Service
public class UserMetierImpl implements UserMetier{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User saveUser(User u) {
		return userRepository.save(u);
	}

	@Override
	public List<User> listeUser() {
		
		return userRepository.findAll();
	}

	@Override
	public User FindUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

}
