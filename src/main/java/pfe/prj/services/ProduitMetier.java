package pfe.prj.services;

import java.util.List;


import pfe.prj.entities.Produit;

public interface ProduitMetier {
	public Produit saveProduit(Produit p);
	public List<Produit> listeProduit();
	public Produit GetProduitfindBynom(String nom);
	public double GetPlafondById(Long id_produit);
	public List<Long> getClientFromProduct(Long produit_id);
	public List<Long> getPacks(Long produit_id);
}
