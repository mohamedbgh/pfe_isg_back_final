package pfe.prj.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Client;
import pfe.prj.entities.Mouvement;
import pfe.prj.enumeration.Nature;
import pfe.prj.enumeration.Sens;

public interface MouvementMetier {
	public List<Mouvement> getMouvements();
	public List<Mouvement> GetMouvementByCompte(Long code_cpte);
	public List<Mouvement> GetMouvementsByNature(Nature N);
	public List<Mouvement> GetMouvementsBySens(Sens S);
	public List<Mouvement> GetMouvementsByNatureAndSens(Nature N, Sens S);
	public double Getsomme_transac(String nature, String sens,Long code_compte);
	public double Getsomme_transacThisMonth(String nature, String sens,Long code_compte);
	public double Getsomme_transacLastMonth(String nature, String sens,Long code_compte);
	public double Getsomme_transacLast2Months(String nature, String sens,Long code_compte);
	public boolean plafondEgalThisMonth(Client c);
}
