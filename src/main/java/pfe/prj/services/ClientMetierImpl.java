package pfe.prj.services;

import java.util.List;

import pfe.prj.dao.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfe.prj.entities.Client;
@Service
public class ClientMetierImpl implements ClientMetier {
	
	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Client saveClient(Client c) {
		return clientRepository.save(c);
	}

	@Override
	public List<Client> listeClient() {
		return clientRepository.findAll();
	}

	@Override
	public List<Long> getClientProduct(Long client_id) {
		 List<Long> list=clientRepository.getClientProduct(client_id);
		 System.out.println(list.toString());
		 return list;
	}

	@Override
	public List<Long> getClientComptes(Long client_id) {
		List<Long> list=clientRepository.getClientComptes(client_id);
		 System.out.println(list.toString());
		 return list;
	}

}
