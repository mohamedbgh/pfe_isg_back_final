package pfe.prj.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfe.prj.dao.MouvementRepository;
import pfe.prj.entities.Client;
import pfe.prj.entities.Mouvement;
import pfe.prj.entities.Produit;
import pfe.prj.enumeration.Nature;
import pfe.prj.enumeration.Sens;
@Service
public class MouvementMetierImpl implements MouvementMetier{
	
@Autowired
private MouvementRepository mouvementRepository;
@Autowired 
private ClientMetierImpl cltImpl;
	@Override
	public List<Mouvement> getMouvements() {
		
		return mouvementRepository.findAll();
	}

	@Override
	public List<Mouvement> GetMouvementsByNature(Nature N) {
		List<Mouvement> list= mouvementRepository.findByNature(N);
		 for (Mouvement mouvement : list) {
			 System.out.println("******"+ mouvement.toString());
			
		}
		// System.out.println("!!!!!!!!!!!!!!!!!!!!!!"+list);
		 return list;
	}

	@Override
	public List<Mouvement> GetMouvementsBySens(Sens S) {
		List<Mouvement> list= mouvementRepository.findBySens(S);
		 for (Mouvement mouvement : list) {
			 System.out.println("******"+ mouvement.toString());
			
		}
		// System.out.println("!!!!!!!!!!!!!!!!!!!!!!"+list);
		 return list;
	}

	@Override
	public List<Mouvement> GetMouvementsByNatureAndSens(Nature N, Sens S) {
		List<Mouvement> list= mouvementRepository.findByNatureAndSens(N, S);
		 for (Mouvement mouvement : list) {
			 System.out.println("******"+ mouvement.toString());
			
		}
		// System.out.println("!!!!!!!!!!!!!!!!!!!!!!"+list);
		 return list;
	}

	

@Override
public double Getsomme_transac(String nature, String sens,Long code_compte) {
	double d = mouvementRepository.somme_transac(nature,sens,code_compte);

	System.out.println("montant !!!!!!!!!!!!"+d);
		return d;
}

@Override
public double Getsomme_transacThisMonth(String nature, String sens, Long code_compte) {
	double d = mouvementRepository.somme_transacThisMonth(nature,sens,code_compte);

	System.out.println("montant this month " +d);
		return d;
}

@Override
public double Getsomme_transacLastMonth(String nature, String sens, Long code_compte) {
	double d = mouvementRepository.somme_transacLastMonth(nature,sens,code_compte);

	System.out.println("montant Last month " +d);
		return d;
}

@Override
public double Getsomme_transacLast2Months(String nature, String sens, Long code_compte) {
	double d = mouvementRepository.somme_transacLast2Months(nature,sens,code_compte);

	System.out.println("montant Last 2 months " +d);
		return d;
}

@Override
public boolean plafondEgalThisMonth(Client c) {
			
	List<Long> list=cltImpl.getClientProduct(c.getId());
	for (int i = 0; i < list.size(); i++) {
		
	}
	return false;
	
	
}

@Override
public List<Mouvement> GetMouvementByCompte(Long code_cpte) {
	List<Mouvement> list= mouvementRepository.MouvementByCompte(code_cpte);
	 for (Mouvement mouvement : list) {
		 System.out.println("******"+ mouvement.toString());
		
	}
	return list;
}



	}


