package pfe.prj.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import mail.Mail;

@Service
public class EmailService {
	
	@Autowired
	private JavaMailSender javaMailSender;

	public boolean sendmail(Mail mail) {
		SimpleMailMessage message =new SimpleMailMessage();
		message.setTo(mail.getSetTo());
		message.setFrom(mail.getSetFrom());
		message.setSubject(mail.getSubject());
		message.setText(mail.getText());
		 
		javaMailSender.send(message);
		return true;
	}
}
