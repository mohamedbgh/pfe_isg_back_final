package pfe.prj.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pfe.prj.dao.ProduitRepository;
import pfe.prj.entities.Produit;

@Service
public class ProduitMetierImpl implements ProduitMetier{
	@Autowired
	private ProduitRepository produitrepos;

	@Override
	public Produit saveProduit(Produit p) {
		return produitrepos.save(p);
	}

	@Override
	public List<Produit> listeProduit() {
		return produitrepos.findAll();
	}

	@Override
	public Produit GetProduitfindBynom(String nom) {
		return produitrepos.findBynom(nom);
	}

	@Override
	public double GetPlafondById(Long produit_id) {
		double d = produitrepos.GetPlafondById(produit_id);
		System.out.println("le plafond de ce produit " +d);

		return d;
	}

	@Override
	public List<Long> getPacks(Long produit_id) {
		List<Long> list=produitrepos.getPacks(produit_id);
		 System.out.println(list.toString());
		 return list;

	}

	@Override
	public List<Long> getClientFromProduct(Long produit_id) {
		List<Long> list=produitrepos.getClientFromProduct(produit_id);
		 System.out.println(list.toString());
		 return list;
	}
	

}
