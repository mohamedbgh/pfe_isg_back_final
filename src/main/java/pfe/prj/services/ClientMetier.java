package pfe.prj.services;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Client;

public interface ClientMetier {
	public Client saveClient(Client c);
	public List<Client> listeClient();
	public List<Long> getClientProduct(Long client_id);
	public List<Long> getClientComptes(Long client_id);
}
