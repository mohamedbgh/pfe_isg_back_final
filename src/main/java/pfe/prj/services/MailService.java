package pfe.prj.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import mail.Mail;
import pfe.prj.entities.Client;

@Service
public class MailService {

	private JavaMailSender javaMailSender ;


	
	@Autowired
public MailService(JavaMailSender javaMailSender) {
		
		this.javaMailSender = javaMailSender;
	}
	
	public boolean sendMail(Mail mail) {
		SimpleMailMessage email =new SimpleMailMessage();
        email.setTo(mail.getSetTo());
        email.setFrom(mail.getSetFrom());
        email.setSubject(mail.getSubject());
        email.setText(mail.getText());
        
       javaMailSender.send(email);
       return true;
}
	
	
}
