package pfe.prj;

import pfe.prj.controllers.MouvementController;
import pfe.prj.dao.ClientRepository;
import pfe.prj.dao.CompteRepository;
import pfe.prj.dao.MouvementRepository;
import pfe.prj.dao.PackRepository;
import pfe.prj.dao.ProduitRepository;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.Query;


import pfe.prj.dao.UserRepository;
import pfe.prj.entities.Client;
import pfe.prj.entities.Compte;
import pfe.prj.entities.Mouvement;
import pfe.prj.entities.Pack;
import pfe.prj.entities.Produit;
import pfe.prj.entities.User;
import pfe.prj.enumeration.Nature;
import pfe.prj.enumeration.Sens;
import pfe.prj.services.ClientMetierImpl;
import pfe.prj.services.MouvementMetierImpl;
import pfe.prj.services.PackMetierImpl;
import pfe.prj.services.ProduitMetierImpl;

@SpringBootApplication
public class PfefinalApplication implements CommandLineRunner{
	
	
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private MouvementRepository mouvementRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PackRepository packRepository;
	
	@Autowired
	private MouvementMetierImpl mvtImpl;
	@Autowired
	private ClientMetierImpl cltImpl;
	@Autowired
	private ProduitMetierImpl prdImpl;
	@Autowired
	private PackMetierImpl packImpl;
	

	public static void main(String[] args) {
		SpringApplication.run(PfefinalApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");


	
	
	User u1=userRepository.save(new User("medbgh", "0000", "admin"));
    User u2=userRepository.save(new User("nouhabgh", "1111", "marketeur"));
	
	Client c1=new Client("benali","ali",formatter1.parse("1998-10-04"),"ali.benali@gmail.com");
	Produit p1=new Produit(1254L,"carte Paltinium", 80, 750, "carte Paltinium");
	Produit p3=new Produit(4568L, "MasterCard", 120, 500, "MasterCard");
	c1.getProduits().add(p1);
 	p1.getClients().add(c1);
	c1.getProduits().add(p3);
 	p3.getClients().add(c1);
 	clientRepository.save(c1);
 	produitRepository.save(p1);
 	produitRepository.save(p3);
 	
 	
	Client c2=clientRepository.save(new Client("bensalah","salah",formatter1.parse("2000-10-09"),"salah.bensalah@gmail.com"));
	

	Client c3=clientRepository.save(new Client("benahmed", "ahmed", formatter1.parse("1977-05-07"),"ahmed.benahmed@gmail.com"));
	Client c4=clientRepository.save(new Client("bgh", "donia", formatter1.parse("1971-02-09"), "doniabgh@gmail.com"));

	Compte cp1=compteRepository.save(new Compte(1234L,formatter1.parse("2012-05-25"), 69875,c1));
	Compte cp2=compteRepository.save(new Compte(1235L, formatter1.parse("2018-05-20"), 20875,c2));
	Compte cp3=compteRepository.save(new Compte(1236L, formatter1.parse("2018-09-18"), 100875,c2));
	Compte cp4=compteRepository.save(new Compte(1237L, formatter1.parse("2019-01-13"), 11125,c3));
	Compte cp5=compteRepository.save(new Compte(1238L, formatter1.parse("2018-08-09"), 100875,c4));
	
	
		
		

	  
	  Mouvement mv2=mouvementRepository.save(new Mouvement(formatter.parse("23-01-2020 11:15:31"),  Nature.Carte_Platinium, 150, cp1, Sens.CREDIT,"Achat matériels informatiques"));
	  Mouvement mv4=mouvementRepository.save(new Mouvement(formatter.parse("25-05-2019 15:42:09"), Nature.Carte_Platinium, 200, cp1, Sens.CREDIT,"Restaurant"));
	  Mouvement mv5=mouvementRepository.save(new Mouvement(formatter.parse("09-01-2020 10:25:45"),  Nature.Carte_MasterCard, 400, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv6=mouvementRepository.save(new Mouvement(formatter.parse("09-01-2020 15:25:22"),  Nature.Espèce, 300, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv7=mouvementRepository.save(new Mouvement(formatter.parse("19-02-2020 18:25:15"),  Nature.Carte_Platinium, 200, cp1, Sens.CREDIT,"Achat matériels informatiques"));
	  Mouvement mv10=mouvementRepository.save(new Mouvement(formatter.parse("27-05-2020 13:40:18"),  Nature.Carte_Platinium, 100, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv11=mouvementRepository.save(new Mouvement(formatter.parse("10-05-2020 13:40:18"),  Nature.Espèce, 300, cp1, Sens.CREDIT,"Etude"));
	  Mouvement mv12=mouvementRepository.save(new Mouvement(formatter.parse("13-05-2020 13:20:58"),  Nature.Carte_Platinium, 100, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv13=mouvementRepository.save(new Mouvement(formatter.parse("09-05-2020 15:00:17"),  Nature.Carte_Platinium, 550, cp1, Sens.CREDIT,"Achat immobilier"));  
	  
	  Mouvement mv14=mouvementRepository.save(new Mouvement(formatter.parse("23-04-2020 11:20:41"),  Nature.Carte_Platinium, 200, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv15=mouvementRepository.save(new Mouvement(formatter.parse("03-04-2020 12:50:41"),  Nature.Carte_Platinium, 150, cp1, Sens.CREDIT));
	  Mouvement mv16=mouvementRepository.save(new Mouvement(formatter.parse("18-04-2020 15:38:26"),  Nature.Carte_Platinium, 400, cp1, Sens.CREDIT,"Achat immobilier"));
	  
	  Mouvement mv17=mouvementRepository.save(new Mouvement(formatter.parse("22-03-2020 11:10:48"),  Nature.Carte_Platinium, 150, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv18=mouvementRepository.save(new Mouvement(formatter.parse("01-03-2020 12:50:21"),  Nature.Carte_Platinium, 450, cp1, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv19=mouvementRepository.save(new Mouvement(formatter.parse("16-03-2020 15:48:26"),  Nature.Carte_Platinium, 150, cp1, Sens.CREDIT,"Achat immobilier"));
	  
	  Mouvement mv1=mouvementRepository.save(new Mouvement(formatter.parse("20-01-2020 10:15:55"), Nature.Espèce, 1300, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv3=mouvementRepository.save(new Mouvement(formatter.parse("10-01-2020 14:26:50"), Nature.Espèce, 1000, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv8=mouvementRepository.save(new Mouvement(formatter.parse("04-01-2020 09:30:35"),  Nature.Espèce, 1500, cp2, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv9=mouvementRepository.save(new Mouvement(formatter.parse("27-02-2020 13:40:18"),  Nature.Espèce, 800, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv20=mouvementRepository.save(new Mouvement(formatter.parse("27-05-2020 13:40:12"),  Nature.Espèce, 150, cp2, Sens.CREDIT,"Restaurant"));
	  Mouvement mv21=mouvementRepository.save(new Mouvement(formatter.parse("17-04-2020 13:20:20"),  Nature.Carte_Visa, 300, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv22=mouvementRepository.save(new Mouvement(formatter.parse("18-05-2020 10:20:38"),  Nature.Carte_Visa, 300, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv23=mouvementRepository.save(new Mouvement(formatter.parse("20-06-2020 08:20:48"),  Nature.Carte_Visa, 300, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv24=mouvementRepository.save(new Mouvement(formatter.parse("21-05-2020 10:20:58"),  Nature.Espèce, 1300, cp2, Sens.CREDIT,"Achat matériels informatiques"));
	  Mouvement mv25=mouvementRepository.save(new Mouvement(formatter.parse("02-01-2020 11:20:02"),  Nature.Carte_Visa, 150, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv26=mouvementRepository.save(new Mouvement(formatter.parse("18-01-2020 12:30:15"),  Nature.Carte_Visa, 150, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv27=mouvementRepository.save(new Mouvement(formatter.parse("29-02-2020 10:20:18"),  Nature.Espèce, 500, cp2, Sens.CREDIT,"Etude"));
	  Mouvement mv28=mouvementRepository.save(new Mouvement(formatter.parse("19-04-2020 09:28:58"),  Nature.Espèce, 300, cp2, Sens.CREDIT,"Etude"));
	  
	  Mouvement mv29=mouvementRepository.save(new Mouvement(formatter.parse("28-06-2020 15:30:24"),  Nature.Carnet_Chéque_15, 1300, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv30=mouvementRepository.save(new Mouvement(formatter.parse("30-05-2020 12:49:37"),  Nature.Carnet_Chéque_15, 700, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv31=mouvementRepository.save(new Mouvement(formatter.parse("02-04-2020 15:30:24"),  Nature.Carnet_Chéque_15, 500, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv32=mouvementRepository.save(new Mouvement(formatter.parse("02-06-2020 09:59:07"),  Nature.Carnet_Chéque_15, 250, cp4, Sens.CREDIT,"Payer facture"));
	  Mouvement mv33=mouvementRepository.save(new Mouvement(formatter.parse("15-05-2020 08:10:54"),  Nature.Carnet_Chéque_15, 1000, cp4, Sens.CREDIT,"Achat matériels informatiques"));
	  Mouvement mv34=mouvementRepository.save(new Mouvement(formatter.parse("18-06-2020 11:17:29"),  Nature.Espèce, 100, cp4, Sens.CREDIT,"Payer Facture"));
	  Mouvement mv35=mouvementRepository.save(new Mouvement(formatter.parse("05-04-2020 08:10:54"),  Nature.Carnet_Chéque_15, 1100, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv36=mouvementRepository.save(new Mouvement(formatter.parse("08-06-2020 10:10:38"),  Nature.Carnet_Chéque_15, 150, cp4, Sens.CREDIT,"Restaurant"));
	  Mouvement mv37=mouvementRepository.save(new Mouvement(formatter.parse("18-06-2020 15:10:54"),  Nature.Carnet_Chéque_15, 300, cp4, Sens.CREDIT,"Achat immobilier"));
	  Mouvement mv38=mouvementRepository.save(new Mouvement(formatter.parse("20-05-2020 09:10:34"),  Nature.Carnet_Chéque_15, 500, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv39=mouvementRepository.save(new Mouvement(formatter.parse("11-04-2020 16:00:04"),  Nature.Carnet_Chéque_15, 400, cp4, Sens.CREDIT,"Payer facture"));
	  Mouvement mv40=mouvementRepository.save(new Mouvement(formatter.parse("10-06-2020 09:50:31"),  Nature.Carnet_Chéque_15, 1000, cp4, Sens.CREDIT,"Achat matieriels informatiques"));
	  Mouvement mv41=mouvementRepository.save(new Mouvement(formatter.parse("29-03-2020 08:10:34"),  Nature.Carnet_Chéque_15, 350, cp4, Sens.CREDIT,"Payer facture"));
	  Mouvement mv42=mouvementRepository.save(new Mouvement(formatter.parse("15-04-2020 15:19:37"),  Nature.Carnet_Chéque_15, 400, cp4, Sens.CREDIT,"Investissement"));
	  Mouvement mv43=mouvementRepository.save(new Mouvement(formatter.parse("09-06-2020 09:01:17"),  Nature.Carnet_Chéque_15, 200, cp4, Sens.CREDIT,"Restaurant"));
	  
	    Produit p2=produitRepository.save(new Produit(4567L,"carte Visa", 100, 300, " carte Visa"));
	    c2.getProduits().add(p2);
		clientRepository.save(c2);
		
		
		Produit p4=produitRepository.save(new Produit(4569L, "crédit auto", 15000, "crédit auto"));
		Produit p5=produitRepository.save(new Produit(4570L, "carte Infinty", 250, 1000, "carte Infinty"));
	
		Produit p6=produitRepository.save(new Produit(4100L,"Carnet_Chéque_15", 30, "Carnet_Chéque_15"));
		c3.getProduits().add(p6);
		clientRepository.save(c3);
		Produit p7=produitRepository.save(new Produit(4101L,"Carnet_Chéque_20", 40, "Carnet_Chéque_20"));
		Produit p8=produitRepository.save(new Produit(4102L,"Carnet_Chéque_30", 60, "Carnet_Chéque_30"));
		c4.getProduits().add(p8);
		clientRepository.save(c4);
		Produit p9=produitRepository.save(new Produit(4103L,"Crédit auto", 10000, "Crédit auto"));
		Produit p10=produitRepository.save(new Produit(4104L,"Crédit immobilier", 25000, "Crédit immobilier"));
		Produit p11=produitRepository.save(new Produit(4105L,"Crédit investissement", 35000, "Crédit investissement"));
		Produit p12=produitRepository.save(new Produit(4106L,"Crédit mariage", 35000, "Crédit mariage"));

		
		
	Pack pack1=packRepository.save(new Pack(1000L, "Pack1", 30000, "Pack1"));
	pack1.getProduits().add(p1);
	pack1.getProduits().add(p4);
	pack1.getProduits().add(p6);
	packRepository.save(pack1);
	
	Pack pack2=packRepository.save(new Pack(1100L, "Pack2", 20000, "Pack2"));
	pack2.getProduits().add(p1);
	pack2.getProduits().add(p3);
	pack1.getProduits().add(p1);
	pack2.getProduits().add(p7);
	packRepository.save(pack2);
	
	
	
//	mvtImpl.GetMouvementsByNature(Nature.Carte);
//	mvtImpl.GetMouvementsBySens(Sens.CREDIT);
	//mvtImpl.GetMouvementsByNatureAndSens(Nature.Carte_Platinium, Sens.CREDIT);
	//mvtImpl.Getsomme_transac(Nature.Carte_Platinium.toString(), Sens.CREDIT.toString(),1234L);
	//cltImpl.getClientProduct(3L);
	//mvtImpl.Getsomme_transacThisMonth(Nature.Carte_Platinium.toString(), Sens.CREDIT.toString(),1234L);
	//mvtImpl.Getsomme_transacLastMonth(Nature.Carte_Platinium.toString(), Sens.CREDIT.toString(),1234L);
	//mvtImpl.Getsomme_transacLast2Months(Nature.Carte_Platinium.toString(), Sens.CREDIT.toString(),1234L);
	
	//prdImpl.GetPlafondById(1254L);
	//cltImpl.getClientComptes(3L);
     //packImpl.getPackProduct(1000L);
     // prdImpl.getPacks(1254L);
      //prdImpl.getClientFromProduct(1254L);
      //mvtImpl.GetMouvementByCompte(1234L);
	
	}

}
