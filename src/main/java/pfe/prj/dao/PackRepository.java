package pfe.prj.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Pack;

public interface PackRepository extends JpaRepository<Pack, Long>{
	//public Pack findbynom (String nom);
	@Query(value="SELECT produit_id FROM pack_produit  WHERE id_pack = ?1" , nativeQuery = true)
	public List<Long> getPackProduct(Long pack_id);
}
