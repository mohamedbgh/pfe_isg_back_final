package pfe.prj.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import pfe.prj.entities.Compte;

public interface CompteRepository extends JpaRepository<Compte, Long>{

}
