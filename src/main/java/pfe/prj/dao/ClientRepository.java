package pfe.prj.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Client;
import pfe.prj.entities.Mouvement;

public interface ClientRepository extends JpaRepository<Client, Long>{
	
	@Query(value="SELECT produit_id FROM client_produit  WHERE client_id = ?1" , nativeQuery = true)
	public List<Long> getClientProduct(Long client_id);
	
	@Query(value="SELECT * FROM compte  WHERE code_cli = ?1" , nativeQuery = true)
	public List<Long> getClientComptes(Long client_id);
}
