package pfe.prj.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import pfe.prj.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

	public User findByUsername(String Username);
	
}
