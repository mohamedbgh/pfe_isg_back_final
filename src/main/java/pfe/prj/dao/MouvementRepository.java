package pfe.prj.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Mouvement;
import pfe.prj.enumeration.Nature;
import pfe.prj.enumeration.Sens;

public interface MouvementRepository extends JpaRepository<Mouvement, Long>{


	public List<Mouvement> findByNature(Nature N);
	public List<Mouvement> findBySens(Sens S);
	public List<Mouvement> findByNatureAndSens(Nature N, Sens S);
	@Query(value="SELECT * FROM mouvement WHERE code_cpte= ?1",nativeQuery = true)
	public List<Mouvement> MouvementByCompte(Long code_cpte);
	 
	
	@Query(value="SELECT coalesce(SUM(m.montant),0) FROM mouvement m WHERE nature = ?1 AND sens = ?2 AND code_cpte = ?3" , nativeQuery = true)
	public double somme_transac(String nature, String sens,Long code_compte);
	
	@Query(value="SELECT coalesce(SUM(m.montant),0) FROM mouvement m WHERE nature = ?1 AND sens = ?2 AND code_cpte = ?3  AND MONTH(m.date_operation ) = MONTH(NOW()) AND YEAR(m.date_operation) = Year(NOW()) ", nativeQuery = true)
	public double somme_transacThisMonth(String nature, String sens,Long code_compte);
	
	@Query(value="SELECT coalesce(SUM(m.montant),0) FROM mouvement m WHERE nature = ?1 AND sens = ?2 AND code_cpte = ?3  AND MONTH(m.date_operation ) = MONTH(NOW()-1) AND YEAR(m.date_operation) = Year(NOW()) ", nativeQuery = true)
	public double somme_transacLastMonth(String nature, String sens,Long code_compte);
	
	@Query(value="SELECT coalesce(SUM(m.montant),0) FROM mouvement m WHERE nature = ?1 AND sens = ?2 AND code_cpte = ?3  AND MONTH(m.date_operation ) = MONTH(NOW()-2) AND YEAR(m.date_operation) = Year(NOW()) ", nativeQuery = true)
	public double somme_transacLast2Months(String nature, String sens,Long code_compte);
	
	@Query(value="SELECT DISTINCT nature FROM mouvement" ,nativeQuery = true)
	public List<String> allNature();
}
