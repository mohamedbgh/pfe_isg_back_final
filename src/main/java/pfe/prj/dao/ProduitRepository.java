package pfe.prj.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pfe.prj.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long>{
	public Produit findBynom(String nom);
	
	@Query(value="SELECT plafond FROM Produit WHERE produit_id = ?1")
	public double GetPlafondById(Long id_produit);
	
	@Query(value="SELECT client_id FROM client_produit  WHERE produit_id = ?1" , nativeQuery = true)
	public List<Long> getClientFromProduct(Long produit_id);
	
	@Query(value="SELECT id_pack FROM pack_produit  WHERE produit_id = ?1" , nativeQuery = true)
	public List<Long> getPacks(Long produit_id);
}
