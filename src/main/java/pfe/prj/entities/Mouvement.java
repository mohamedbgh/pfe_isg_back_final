package pfe.prj.entities;


import java.rmi.server.Operation;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import pfe.prj.entities.Compte;
import pfe.prj.enumeration.Nature;
import pfe.prj.enumeration.Sens;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Mouvement {
	@Id @GeneratedValue
	private Long numero;
	@Column
	@NotNull
	@JsonFormat(pattern="dd-MM-yyyy ")
	private Date dateOperation;
	@Column
	@NotNull
	@Enumerated(EnumType.STRING)
	private Nature nature;
	@Column
	@NotNull
	private double montant;
	@Column
	@NotNull
	@Enumerated(EnumType.STRING)
	private Sens sens ;
	@Column
	private String operation;
	
	@ManyToOne
	@JoinColumn(name = "CODE_CPTE")
	private Compte compte;
	
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public Date getDateOperation() {
		return dateOperation;
	}
	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	
	public Sens getSens() {
		return sens;
	}
	public void setSens(Sens sens) {
		this.sens = sens;
	}
	
	public Nature getNature() {
		return nature;
	}
	public void setNature(Nature nature) {
		this.nature = nature;
	}
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		operation = operation;
	}
	public Mouvement() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Mouvement( @NotNull Date dateOperation, @NotNull Nature nature,@NotNull double montant,@NotNull Compte compte,@NotNull Sens sens, String operation) {
		super();
		this.dateOperation = dateOperation;
		this.nature = nature;
		this.montant = montant;
		this.compte = compte;
		this.sens = sens;
		this.operation = operation;
	}
	
	
	public Mouvement(@NotNull Date dateOperation, @NotNull Nature nature, @NotNull double montant,
			 Compte compte,@NotNull Sens sens) {
		super();
		
		this.dateOperation = dateOperation;
		this.nature = nature;
		this.montant = montant;
		this.sens = sens;
		this.compte = compte;
	}
	@Override
	public String toString() {
		return "Mouvement [numero=" + numero + ", dateOperation=" + dateOperation + ", nature=" + nature + ", montant="
				+ montant + ", sens=" + sens + "]";
	}
	
	
	
	
}
