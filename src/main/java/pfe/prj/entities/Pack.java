package pfe.prj.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Pack {

	@Id 
	private Long id_pack;
	@Column
	@NotNull
	private String nom;
	@Column
	@NotNull
	private double prix;
	@Column
	@NotNull
	private String description;
	
	 @JsonIgnore
	 @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	    @JoinTable(name = "pack_produit",
	        joinColumns = {
	            @JoinColumn(name = "id_pack")
	        },
	        inverseJoinColumns = {
	            @JoinColumn(name = "produit_id")
	        })
	    private Set < Produit > produits = new HashSet < > ();
	 
	 

	public Pack() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Pack(Long id_pack, @NotNull String nom, @NotNull double prix, @NotNull String description) {
		super();
		this.id_pack = id_pack;
		this.nom = nom;
		this.prix = prix;
		this.description = description;
	}


	public Long getId_pack() {
		return id_pack;
	}

	public void setId_pack(Long id_pack) {
		this.id_pack = id_pack;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Produit> getProduits() {
		return produits;
	}

	public void setProduits(Set<Produit> produits) {
		this.produits = produits;
	}
	
}
