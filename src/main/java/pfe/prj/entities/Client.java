package pfe.prj.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pfe.prj.entities.Compte;
import pfe.prj.entities.Produit;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Client {
	@Id @GeneratedValue
	private Long id;
	@Column
	@NotNull 
	@Size(min = 1, max = 15)
	private String nom;
	@Column
	@NotNull 
	@Size(min = 1, max = 15)
	private String prenom;
	@Column
	@NotNull 
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateNaissance;
	@Column
	@NotNull 
	private String Mail;
	 
	 @JsonIgnore
	 @OneToMany(mappedBy="client",fetch=FetchType.LAZY )
		private List<Compte> comptes ;
//	 
//	 @ManyToMany(cascade = CascadeType.ALL)
//	    @JoinTable(name = "client_produit",
//	        joinColumns = @JoinColumn(name = "client_id", referencedColumnName = "id"),
//	        inverseJoinColumns = @JoinColumn(name = "produit_id", referencedColumnName = "produit_id"))
//	   // private ArrayList<Produit> produits= new ArrayList <Produit>();
//	 private Set < Produit > produits = new HashSet < > ();
	

// @ManyToMany
// @JoinTable(
//   name = "produit_like", 
//   joinColumns = @JoinColumn(name = "id"), 
//   inverseJoinColumns = @JoinColumn(name = "id_produit"))
// private List<Produit> likedproduct ;
//
//
// 
// @ManyToMany(mappedBy = "géré_client")
//	 private List<User> Users;
//	 
	
	 @JsonIgnore
	 @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	    @JoinTable(name = "client_produit",
	        joinColumns = {
	            @JoinColumn(name = "client_id")
	        },
	        inverseJoinColumns = {
	            @JoinColumn(name = "produit_id")
	        })
	    private Set < Produit > produits = new HashSet < > ();
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Client(@NotNull String nom,@NotNull String prenom,@NotNull Date dateNaissance,@NotNull String mail) {
		super();
		this.nom =nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.Mail = mail;
	}
	

	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getMail() {
		return Mail;
	}
	public void setMail(String mail) {
		Mail = mail;
	}



	public List<Compte> getComptes() {
	return comptes;
	}

public void setComptes(List<Compte> comptes) {
		this.comptes = comptes;
		}

public Set<Produit> getProduits() {
	return produits;
}

public void setProduits(Set<Produit> produits) {
	this.produits = produits;
}






	//public Collection<Produit> getProduit() {
			//return produit;
		//}

	//public void setProduit(Collection<Produit> produit) {
		//this.produit = produit;
		//}
	
	
	}
	
	