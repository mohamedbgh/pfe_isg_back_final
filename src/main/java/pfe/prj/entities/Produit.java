package pfe.prj.entities;



import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import pfe.prj.entities.Client;

@Entity
public class Produit{
	@Id 
private Long produit_id;
	@Column
	@NotNull 
private String nom;
	@Column
	@NotNull 
private double prix;
	@Column	
	private double plafond;
	@Column
	@Size( max = 100)
	private String description;
	


//@ManyToMany(mappedBy = "produits")
//private ArrayList<Client> clients = new ArrayList<Client>();
//@ManyToMany(cascade = CascadeType.ALL)
//@JoinTable(name = "client_produit",
//    joinColumns = @JoinColumn(name = "produit_id", referencedColumnName = "id"),
//    inverseJoinColumns = @JoinColumn(name = "client_id", referencedColumnName = "client_id"))
//// private ArrayList<Produit> produits= new ArrayList <Produit>();
//private Set < Client > clients = new HashSet < > ();




//@ManyToMany(mappedBy = "likedproduct")
//private List<Client> Clients;

@JsonIgnore
@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "produits")
private Set < Client > clients = new HashSet < > ();


public Produit(Long produit_id,@NotNull String nom,@NotNull double prix,double plafond,@Size(max = 100) String description) {
	super();
	this.produit_id = produit_id;
	this.nom = nom;
	this.prix = prix;
	this.plafond = plafond;
	this.description = description;
}




public Produit(Long produit_id, @NotNull String nom, @NotNull double prix, @Size(max = 100) String description) {
	super();
	this.produit_id = produit_id;
	this.nom = nom;
	this.prix = prix;
	this.description = description;
}




public Produit() {
	super();
	// TODO Auto-generated constructor stub
}

public Long getProduit_id() {
	return produit_id;
}
public void setProduit_id(Long produit_id) {
	this.produit_id = produit_id;
}


public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}


public double getPrix() {
	return prix;
}
public void setPrix(double prix) {
	this.prix = prix;
}



public double getPlafond() {
	return plafond;
}




public void setPlafond(double plafond) {
	this.plafond = plafond;
}




public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}




public Set<Client> getClients() {
	return clients;
}




public void setClients(Set<Client> clients) {
	this.clients = clients;
}

















}
