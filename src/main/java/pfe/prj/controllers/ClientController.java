package pfe.prj.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pfe.prj.dao.ClientRepository;
import pfe.prj.dao.CompteRepository;
import pfe.prj.dao.ProduitRepository;
import pfe.prj.entities.Client;
import pfe.prj.entities.Compte;
import pfe.prj.entities.Produit;
import pfe.prj.services.ClientMetier;
import pfe.prj.services.ClientMetierImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ClientController {
	
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private CompteRepository compterepos;
	
	@RequestMapping(value = "/Clients", method = RequestMethod.GET)
	public List<Client> getClients(){
		return clientRepository.findAll();
	}

	@RequestMapping(value = "/Client/{id}", method = RequestMethod.GET)
	public Client getClient(@PathVariable Long id){
		return clientRepository.findById(id).get();
	}
//	@RequestMapping(value = "/Clients/{username}", method = RequestMethod.GET)
//	public Client getClientByUserame(@PathVariable String username){
//		return clientRepository.findByUsername(username);
//	}
	

	@RequestMapping(value = "/Clients", method = RequestMethod.POST)
	public Client save(@RequestBody Client C){
		
		return clientRepository.save(C);
	}
	
	@RequestMapping(value = "/Clients/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		 clientRepository.deleteById(id);
		 return true;
	}
	
	
	@RequestMapping(value = "/Clients/{id}", method = RequestMethod.PUT)
	public Client save(@PathVariable Long id,@RequestBody Client C){
		C.setId(id);
		return clientRepository.save(C);
	}
	@RequestMapping(value = "/ClientProduct/{id}", method = RequestMethod.GET)
		public List<Optional<Produit>> getProduitForClient(@PathVariable Long id){
		
			List <Long> produitsId = clientRepository.getClientProduct(id);
			List <Optional<Produit>> produits = new ArrayList();
			for (Long idp : produitsId) {
				produits.add(produitRepository.findById(idp));
			}
			return produits;
	}
	@RequestMapping(value = "/ClientCompte/{id}", method = RequestMethod.GET)
	public List<Optional<Compte>> getCompteForClient(@PathVariable Long id){
	
		List <Long> comptesId = clientRepository.getClientComptes(id);
		List <Optional<Compte>> comptes = new ArrayList();
		for (Long idp : comptesId) {
			comptes.add(compterepos.findById(idp));
		}
		return comptes;
}
	
  
}