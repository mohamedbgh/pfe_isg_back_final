package pfe.prj.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import pfe.prj.dao.ClientRepository;
import pfe.prj.dao.CompteRepository;
import pfe.prj.entities.Client;
import pfe.prj.entities.Compte;

@RestController
@CrossOrigin
public class CompteController {

	@Autowired
	private CompteRepository Compterepos ;
	@Autowired
	private ClientRepository clientrepos ;
	
	@RequestMapping(value = "/Comptes", method = RequestMethod.GET)
	public List<Compte> getComptes(){
		return Compterepos.findAll();
	}

	
	@RequestMapping(value = "/Compte/{id}", method = RequestMethod.GET)
public Compte getClient(@PathVariable Long id){
	return Compterepos.findById(id).get();
}
	
	@RequestMapping(value = "/CompteAdd/{id}", method = RequestMethod.POST)
	public Compte create(@PathVariable Long id,@RequestBody Compte c){
		System.out.println("********************"+id+"*********"+c.toString());
		Client client= clientrepos.findById(id).get();
		c.setClient(client);
		System.out.println("!!!!!!!!!!!!!!!!!!!"+c.toString());
		return Compterepos.save(c);
	}
	
	@RequestMapping(value = "/ComptetDeletebyId/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		 Compterepos.deleteById(id);
		 return true;
}
	
	@RequestMapping(value = "/CompteUpdate/{id}", method = RequestMethod.PUT)
	public Compte save(@PathVariable Long id,@RequestBody Compte C){
		C.setCodeCompte(id);
		return Compterepos.save(C);
	}
}

