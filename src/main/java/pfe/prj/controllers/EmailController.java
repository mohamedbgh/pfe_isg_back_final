package pfe.prj.controllers;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mail.EmailCfg;
import mail.Mail;
import pfe.prj.entities.Client;
import pfe.prj.services.EmailService;


@RestController
@CrossOrigin
public class EmailController {

	public EmailCfg emailCfg;
    @Autowired
    private EmailService emailService;
    
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    public boolean sendMessage(@RequestBody Mail mail){
try {
	return emailService.sendmail(mail);
} catch (MailException e) {
	return false;
}


   

//	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
//    public void sendMessage(@RequestBody Mail mail,
//                             BindingResult bindingResult){
//        if(bindingResult.hasErrors()){
//            throw new ValidationException("message is not valid");
//        }
//
//        // Create a mail sender
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost(this.emailCfg.getHost());
//        mailSender.setPort(this.emailCfg.getPort());
//        mailSender.setUsername(this.emailCfg.getUsername());
//        mailSender.setPassword(this.emailCfg.getPassword());
//
//        // Create an email instance
//        SimpleMailMessage mailMessage = new SimpleMailMessage();
//        mailMessage.setFrom(mail.getEmail());
//        mailMessage.setTo("mbgh@gmail.com");
//        mailMessage.setSubject("New feedback from " + mail.getName());
//        mailMessage.setText(mail.getMessage());
//
//        // Send mail
//        mailSender.send(mailMessage);
//    }
}
}
