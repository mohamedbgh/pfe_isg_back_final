package pfe.prj.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pfe.prj.dao.PackRepository;
import pfe.prj.dao.ProduitRepository;
import pfe.prj.entities.Pack;
import pfe.prj.entities.Produit;

@RestController
@CrossOrigin
public class PackController {
	@Autowired
	private PackRepository packRepository;
	@Autowired
	private ProduitRepository produitRepository;
		
		@RequestMapping(value = "/Packs", method = RequestMethod.GET)
		public List<Pack> getPacks(){
			return packRepository.findAll();
		}

		@RequestMapping(value = "/Pack/{id}", method = RequestMethod.GET)
		public Pack getPack(@PathVariable Long id){
			return packRepository.findById(id).get();
		}
//		@RequestMapping(value = "/Packs/{nom}", method = RequestMethod.GET)
//		public Pack getpackByname(@PathVariable String nom){
//			return packRepository.findbyname(nom);
//		}
		

		@RequestMapping(value = "/Packs", method = RequestMethod.POST)
		public Pack create(@RequestBody Pack P){
			return packRepository.save(P);
		}
		
		@RequestMapping(value = "/Packs/{id}", method = RequestMethod.DELETE)
		public boolean supprimer(@PathVariable Long id){
			 packRepository.deleteById(id);
			 return true;
		}
		
		
		@RequestMapping(value = "/Packs/{id}", method = RequestMethod.PUT)
		public Pack save(@PathVariable Long id,@RequestBody Pack P){
			P.setId_pack(id);
			return packRepository.save(P);
		}
		@RequestMapping(value = "/PackProduct/{id}", method = RequestMethod.GET)
		public List<Optional<Produit>> getProduitFromPack(@PathVariable Long id){
		
			List <Long> produitsId = packRepository.getPackProduct(id);
			List <Optional<Produit>> produits = new ArrayList();
			for (Long idp : produitsId) {
				produits.add(produitRepository.findById(idp));
			}
			return produits;
	}

}
