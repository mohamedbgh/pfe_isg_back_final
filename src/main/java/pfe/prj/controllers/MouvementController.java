package pfe.prj.controllers;



import java.io.Console;
import java.util.List;

import pfe.prj.dao.MouvementRepository;
import pfe.prj.entities.Compte;
import pfe.prj.entities.Mouvement;
import pfe.prj.enumeration.Nature;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class MouvementController {
	
	@Autowired
	private MouvementRepository mouvementrepos;
	
	
	@RequestMapping(value = "/Mouvements", method = RequestMethod.GET)
	public List<Mouvement> getMouvements(){
		return mouvementrepos.findAll();
	}
	@RequestMapping(value = "/Mouvements/{code_cpte}", method = RequestMethod.GET)
	public List<Mouvement> getMouvementCompte(@PathVariable Long code_cpte){
		return mouvementrepos.MouvementByCompte(code_cpte);
	}
	
	//@RequestMapping(value = "/Mouvements/{Nature}", method = RequestMethod.GET)
//	public List<Mouvement> GetMouvementsByNature( Nature n){
//		
//		 List<Mouvement> list= mouvementrepos.findByNature(n);
//		 for (Mouvement mouvement : list) {
//			 System.out.println("******"+ mouvement.toString());
//			
//		}
//		// System.out.println("!!!!!!!!!!!!!!!!!!!!!!"+list);
//		 return list;
		 
		
		
	
	
	
	
//	@RequestMapping(value = "/Mouvement/{codeCompte}" , method = RequestMethod.GET)
//	public List<Mouvement> getMouvementparCompte(@PathVariable Long codeCompte){
//		Compte cp= mvIpml.getCompte(codeCompte);
//		return Compte.getMouvements();
//		
//	}
	
@RequestMapping(value = "/Mouvements/allnature", method = RequestMethod.GET)
public List<String> getAllNature(){
	return mouvementrepos.allNature();
}

}


